int PIRpin = 8; // PIR connected pin
int ledPin = 2;
int value;
 
void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT); //For Led
  //pinMode(PIRpin, INPUT);
}
 
void loop() {
  value = digitalRead(PIRpin); //Read PIR state
  
  if (value == LOW) {
    Serial.println("No Movement"); 
    digitalWrite (ledPin, LOW);
  }
  else {
    Serial.println("Movement!"); 
    digitalWrite (ledPin, HIGH);                
 
  }
  
  delay(100); //Wait
}
